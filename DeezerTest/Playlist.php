<?php

/**
 * Class Playlist
 */
class Playlist
{

    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $user;

    /**
     * @var integer
     */
    protected $song;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

}
