<?php

/**
 * Class Database
 */
class Database extends PDO
{
    /**
     * @var array
     */
    protected $messageArray;

    /**
     * Database constructor.
     * @param $dsn
     * @param $username
     * @param $passwd
     * @param $options
     */
    public function __construct($dsn, $username, $passwd, $options)
    {
        parent::__construct($dsn, $username, $passwd, $options);
    }

    /**
     * @param $table
     * @param $entity : serialized entity
     * @param null $where
     */
    public function post($entity)
    {
        $query = $this->getInsertQueryFromEntity($entity);
        $request = $this->prepare($query);
        try  {
            $request->execute();
        } catch(Exception $e) {
            echo "[".$e->getCode()."] ".$e->getMessage();
            $this->rollBack();
        }
    }

    /**
     * Converts an entity to a full insert query
     *
     * @param $entity
     * @return string
     */
    public function getInsertQueryFromEntity($entity)
    {
        $values = "";

        switch (get_class($entity)) {
            case 'User':
                $table = 'user (name, email)';
                break;
            case 'Song' :
                $table = 'song (title, length)';
                break;
            default :
                $table = 'user (name, email)';
                break;
        }

        $entity = (array) $entity; // Cast the entity as array so attributes are easier to manipulate
        unset($entity[key($entity)]); // We never send the id (the first element) as it is auto increment in MySQL
        $values = implode('\',\'',$entity);

        return "INSERT INTO $table VALUES ('$values')";
    }

    /**
     * This function retrieves either the list of entities or one entity by id.
     *
     * @param $entity
     * @param null $id
     * @return array
     */
    public function get($entity, $id=null)
    {
        $query = "SELECT * FROM $entity";
        $queryWhere = $id ? $query.=" WHERE id = $id" : $query; // Imitate the behavior of findOneById. Add the where id logic if $id exists
        $request = $this->prepare($queryWhere);
        $request->execute();

        return $request->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Returns true if user has song in his favourites, false else.
     *
     * @param $user
     * @param $song
     * @return bool
     */
    public function userHasSong($user, $song)
    {
        $query = "SELECT COUNT(*) AS hasSong FROM favourite_songs WHERE user_id=$user AND song_id=$song";
        $request = $this->prepare($query);
        $request->execute();

        $result = (int) ($request->fetchColumn(0)); // Will return 0 if no match, 1 if there's a match
        if($result) {
            return true;
        }

        return false;
    }

    /**
     * Add a song to a user
     *
     * @param $user
     * @param $song
     * @return array
     */
    public function addSongToUser($user, $song)
    {
        if($this->userHasSong($user, $song)) {
            $this->errorArray = [
                'error' => true,
                'message' => 'The user already has this song in his favourites',
            ];

            returnForOutput('json', $this->errorArray);
        }

        $query = "INSERT INTO favourite_songs(user_id, song_id) VALUES ($user, $song)";
        $request = $this->prepare($query);
        $request->execute();
        $this->returnSuccessMessage();
    }

    /**
     * Removes a song from a user
     *
     * @param $user
     * @param $song
     * @return array
     */
    public function removeSongFromUser($user, $song)
    {
        if(!$this->userHasSong($user, $song)) {
            $this->messageArray = [
                'error' => true,
                'message' => 'The user does not have this song in his favourites',
            ];

            return $this->messageArray;
        }

        $query = "DELETE FROM favourite_songs WHERE user_id = $user AND song_id = $song";
        $request = $this->prepare($query);
        $request->execute();
        $this->returnSuccessMessage();
    }

    /**
     * Get songs from a user
     *
     * @param $user
     * @return array
     */
    public function getUserSongs($user)
    {
        $query = "SELECT * FROM favourite_songs fs LEFT JOIN song s ON fs.song_id = s.id WHERE fs.user_id = $user";
        $request = $this->prepare($query);
        $request->execute();

        return $request->fetchAll(PDO::FETCH_ASSOC);
    }

    protected function returnSuccessMessage()
    {
        $this->messageArray = [
            'error' => false,
            'message' => 'Success',
        ];

        returnForOutput('json', $this->messageArray);
    }
}
