<?php

    if(isset($_POST['action']) && isset($_POST['user']) && isset($_POST['song'])) {
        $action = $_POST['action'];
        $user = (int) $_POST['user'];
        $song = (int) $_POST['song'];

        switch ($action) {
            case 'add':
                $results = $database->addSongToUser($user, $song);
                returnForOutput($output, $results);
                break;
            case 'remove':
                $results = $database->removeSongFromUser($user, $song);
                returnForOutput($output, $results);
                break;
            default : break;
        }

    }
?>

<form action="" method="POST" name="addFavouriteSong">
    <select name="action">
        <option value="remove">Remove</option>
        <option value="add">Add</option>
    </select>
    song
    <select name="song">
        <?php foreach($songs as $song) {
            echo '<option value="'.$song['id'].'">'.$song['title'].'</option>';
        }
        ?>

    </select>
    to user
    <select name="user">
        <?php foreach($users as $user) {
            echo '<option value="'.$user['id'].'">'.$user['name'].'</option>';
        }
        ?>
    </select>

    <input type="submit" value="GO" />
</form>

<hr />

See songs of a user : <br />

<?php

foreach($users as $user) {
    echo '<a href="?entity=favourite_songs&id='.$user['id'].'">'.$user['name'].'</a><br />';
}

?>
