**Ce projet est le sujet 1 du test Deezer**

* Le fichier deezer_test.sql est la BDD contenant quelques fixtures
* La page d'accueil est la page ?page=home

Si j'avais eu plus de temps ce test, j'aurai davantage mis en avant l'aspect sécurité (sanitization des variables GET, POST etc)

Le schéma BDD :

user
----

id

name (varchar 255)

email (varchar 255)



song
----

id

name (varchar 255)

length (int)


favourite_songs
----

id

user_id (integer) <-- refers to user.id

song_id (integer) <-- refers to song.id

** Le projet 2 se situe dans le dossier /SDK/ **