<?php

spl_autoload_register(function ($class) {
    include 'DeezerTest/' . $class . '.php';
});

$users = $songs = [];
$database = new Database('mysql:host=localhost;dbname=local_api', 'root', '', []);

$entity = isset($_GET['entity']) ? $_GET['entity'] : null;
$page = isset($_GET['page']) ? $_GET['page'] : null;
$id = isset($_GET['id']) ? (int) $_GET['id'] : null;
$output = isset($_GET['output']) ? $_GET['output'] : 'json';

$allowedEntities = ['user', 'song'];


if(isset($entity) && (in_array($entity, $allowedEntities)))
{
    $results = $database->get($entity, $id);
    returnForOutput($output, $results);
} if(isset($entity) && $entity === 'favourite_songs' && isset($id)) {
    $results = $database->getUserSongs($id);
    returnForOutput($output, $results);
} else {

    $songs = $database->get('song');
    $users = $database->get('user');

    if($page === 'home') {
        include('views/main.html.php');
    }
    if($page === 'favouriteSongs') {
        include('views/addSong.html.php');
    }
}


/**
 * Returns data in specified format (by default, json)
 *
 * @param $output
 * @param $results
 */
function returnForOutput($output, $results)
{
    if($output === 'json') {
        header('Content-Type: application/json');
        echo json_encode($results);
        die(); // Prevents any html to be loaded after.
    }
}

?>
